// SPDX-License-Identifier: MIT
pragma solidity ^0.7.2;

import "./Randomizer.sol";
import "./ComplexOwnable.sol";

/// @title This contract generate token name and control arrays with name.
/// @author Oleksandr Fedorenko
contract NameGenerator is ComplexOwnable, Randomizer {
    /// @notice Contain add first token name
    string[] public name1;

    /// @notice Contain add last token name
    string[] public name2;

    constructor() {
        name1.push("Cool");
        name1.push("Nasty");
        name1.push("Great");
        name1.push("Mad");
        name1.push("Crazy");
        name1.push("Small");
        name1.push("Beautiful");
        name1.push("Dumb");
        name1.push("Weird");
        name1.push("Square");
        name1.push("Strong");
        name1.push("Weak");
        name1.push("Brave");
        name1.push("Cowardly");
        name1.push("Determined");
        name1.push("Stinky");
        name1.push("Greedy");
        name1.push("Exciting");
        name1.push("Business");
        name1.push("Religious");
        name1.push("Plastic");
        name1.push("Orange");
        name1.push("High");
        name1.push("Catchy");
        name1.push("Stable");
        name1.push("Soft");
        name1.push("Vocal");
        name1.push("Cold");
        name1.push("Sizzling");
        name1.push("Bomb");
        name1.push("Chronic");
        name1.push("Epic");
        name1.push("Fast");
        name1.push("Amusing");
        name1.push("Bright");
        name1.push("Tattered");
        name1.push("Crying");
        name1.push("Compulsive");
        name1.push("Thirsty");
        name1.push("Exhausted");
        name1.push("Watery");
        name1.push("Obstinate");
        name1.push("Spiky");

        name2.push("fanatic");
        name2.push("fighter");
        name2.push("horse");
        name2.push("goblin");
        name2.push("bastard");
        name2.push("cannibal");
        name2.push("director");
        name2.push("student");
        name2.push("master");
        name2.push("light");
        name2.push("tube");
        name2.push("cap");
        name2.push("glass");
        name2.push("airplane");
        name2.push("pencil");
        name2.push("sock");
        name2.push("graphite");
        name2.push("tomato");
        name2.push("disc");
        name2.push("eye");
        name2.push("tongue");
        name2.push("hammer");
        name2.push("barrel");
        name2.push("bug");
        name2.push("ficus");
        name2.push("cactus");
        name2.push("bicycle");
        name2.push("cat");
        name2.push("parrot");
        name2.push("devil");
        name2.push("baby");
        name2.push("dad");
        name2.push("hump");
        name2.push("mandarin");
        name2.push("smoke");
        name2.push("chicken");
        name2.push("muffin");
        name2.push("tree");
        name2.push("worm");
        name2.push("ball");
        name2.push("cleaver");
        name2.push("tobacco");
        name2.push("aspirin");
    }

    /// @notice Generate random name for token.
    /// @return Token name
    function getCeroName() internal view returns (string memory) {
        return string(abi.encodePacked(name1[random(name1.length, 1)], " ", name2[random(name2.length, 2)]));
    }

    /// @notice Add new name to name1.
    /// @param _newName New name
    function setName1(string memory _newName) external onlyOwner {
        name1.push(_newName);
    }

    /// @notice Add new name to name2.
    /// @param _newName New name
    function setName2(string memory _newName) external onlyOwner {
        name2.push(_newName);
    }
}
