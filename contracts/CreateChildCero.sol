// SPDX-License-Identifier: MIT
pragma solidity ^0.7.2;
pragma experimental ABIEncoderV2;

import "@openzeppelin/contracts/math/SafeMath.sol";
import "@openzeppelin/contracts/math/Math.sol";
import "./interfaces/ICeroStruct.sol";
import "./CeroToken.sol";
import "./services/Randomizer.sol";
import "./services/BalanceManagement.sol";

/// @title This contract generate new token (child) from 2 other token.
/// @author Oleksandr Fedorenko
contract CreateChildCero is ICeroStruct, Randomizer, BalanceManagement {
    using SafeMath for uint256;
    using Math for uint256;

    /// @notice Contract address which contain tokens and tokens data.
    CeroToken public tokenContract;

    constructor(address _tokenContract) {
        tokenContract = CeroToken(_tokenContract);
    }

    event ChildCreate(uint256 indexed newChild, uint256 indexed parent1, uint256 indexed parent2);

    /// @notice Token stats struct
    /// @param st Token strength
    /// @param st Token protection
    /// @param st Token agility
    /// @param st Token magic
    struct CeroStats {
        uint256 st;
        uint256 pr;
        uint256 ag;
        uint256 ma;
    }

    /// @notice Create child token. Make 2 parent token not a child.
    /// @param _cero1Num Parent token id to merge
    /// @param _cero2Num Parent token id to merge
    function createChildCero(uint256 _cero1Num, uint256 _cero2Num) external {
        require(_cero1Num != _cero2Num, "[ECCC-106] - The numbers must not match.");
        require(
            tokenContract.ownerOf(_cero1Num) == msg.sender && tokenContract.ownerOf(_cero2Num) == msg.sender,
            "[ECCC-101] - You are not a token owner."
        );

        Cero memory _cero1 = tokenDataToTokenStruct(_cero1Num);
        Cero memory _cero2 = tokenDataToTokenStruct(_cero2Num);
        require(_cero1.lvl == _cero2.lvl, "[ECCC-102] - Must be on the same level.");
        require(_cero1.isChild && _cero2.isChild, "[ECCC-105] - There should be no descendants.");

        CeroStats memory _newStats = _getNewStats(_cero1, _cero2);
        tokenContract.createToken(
            msg.sender,
            _cero1.lvl + 1,
            uint16(_newStats.st),
            uint16(_newStats.pr),
            uint16(_newStats.ag),
            uint16(_newStats.ma),
            _cero1Num,
            _cero2Num,
            true
        );

        tokenContract.updateTokenChildStatus(_cero1Num, false);
        tokenContract.updateTokenChildStatus(_cero2Num, false);

        emit ChildCreate(tokenContract.getTokensCount() - 1, _cero1Num, _cero2Num);
    }

    /// @notice Form new token characteristics after merging
    /// @param _cero1 Token data
    /// @param _cero2 Token data
    /// @return _ceroStats New token characteristics
    function _getNewStats(Cero memory _cero1, Cero memory _cero2) private view returns (CeroStats memory _ceroStats) {
        // Get sum of parent characteristics
        CeroStats memory _statsSum =
            CeroStats({
                st: uint256(_cero1.strength).add(_cero2.strength),
                pr: uint256(_cero1.protection).add(_cero2.protection),
                ag: uint256(_cero1.agility).add(_cero2.agility),
                ma: uint256(_cero1.magic).add(_cero2.magic)
            });

        // Calculate type point and reset some of them to 0
        uint256 _warriorPoint = _statsSum.st + _statsSum.pr;
        uint256 _thiefPoint = _statsSum.st + _statsSum.ag;
        uint256 _wizardPoint = _statsSum.ma + _statsSum.ag;
        _warriorPoint > _wizardPoint || _thiefPoint > _wizardPoint ? _statsSum.ma = 0 : _statsSum.st = 0;

        // Calculate total point sum
        uint256 _totalSum = _statsSum.st.add(_statsSum.pr).add(_statsSum.ag).add(_statsSum.ma);

        uint256 _pointPerLvl = 11;

        // Calculate new token characteristics for distribution
        CeroStats memory _statsDist =
            CeroStats({
                st: _getStatDistribution(_statsSum.st, _totalSum, _pointPerLvl),
                pr: _getStatDistribution(_statsSum.pr, _totalSum, _pointPerLvl),
                ag: _getStatDistribution(_statsSum.ag, _totalSum, _pointPerLvl),
                ma: _getStatDistribution(_statsSum.ma, _totalSum, _pointPerLvl)
            });

        // Get bigger characteristic value for distribution
        uint256 _maxStat = (_statsDist.st).max(_statsDist.pr).max(_statsDist.ag).max(_statsDist.ma);

        // Get rounding losses by points for distribution
        uint256 _roundingPointLooses =
            _pointPerLvl.sub(_statsDist.st.add(_statsDist.pr).add(_statsDist.ag).add(_statsDist.ma));

        // Add rounding losses to bigger characteristic
        if (_maxStat == _statsDist.st) {
            _statsDist.st = _statsDist.st + _roundingPointLooses;
        } else if (_maxStat == _statsDist.pr) {
            _statsDist.pr = _statsDist.pr + _roundingPointLooses;
        } else if (_maxStat == _statsDist.ag) {
            _statsDist.ag = _statsDist.ag + _roundingPointLooses;
        } else {
            _statsDist.ma = _statsDist.ma + _roundingPointLooses;
        }

        // Validate that pointPerLvl equal point for distribution
        require(
            (_statsDist.st).add(_statsDist.pr).add(_statsDist.ag).add(_statsDist.ma) == _pointPerLvl,
            "[ECCC-104] - Contract error. Wrong distribution sum."
        );

        // Calculate new characteristics
        CeroStats memory _newStats =
            CeroStats({
                st: _formNewStat(_cero1.strength, _cero2.strength, _statsDist.st),
                pr: _formNewStat(_cero1.protection, _cero2.protection, _statsDist.pr),
                ag: _formNewStat(_cero1.agility, _cero2.agility, _statsDist.ag),
                ma: _formNewStat(_cero1.magic, _cero2.magic, _statsDist.ma)
            });

        // Reset some of characteristic to 0
        _warriorPoint > _wizardPoint || _thiefPoint > _wizardPoint ? _newStats.ma = 0 : _newStats.st = 0;

        return _newStats;
    }

    /// @notice Calculate points for distribution for each characteristic.
    /// @param _statSum Sum of characteristics (str or ag or ...)
    /// @param _totalSum Total characteristics sum
    /// @param _pointPerLvl Point for level
    /// @return Points amount
    function _getStatDistribution(
        uint256 _statSum,
        uint256 _totalSum,
        uint256 _pointPerLvl
    ) private pure returns (uint256) {
        return _statSum.mul(_pointPerLvl).div(_totalSum);
    }

    /// @notice Calculate new token characteristic value.
    /// @param _oldStat1 Old token1 characteristic
    /// @param _oldStat2 Old token2 characteristic
    /// @param _statDist Distribution point
    /// @return Characteristic value for new token
    function _formNewStat(
        uint256 _oldStat1,
        uint256 _oldStat2,
        uint256 _statDist
    ) private view returns (uint16) {
        return uint16(random(100, _statDist) < 50 ? _oldStat1 + _statDist : _oldStat2 + _statDist);
    }

    /// @notice Transform data from token contract to memory struct.
    /// @param _tokenNum Token number
    /// @return _token Token data
    function tokenDataToTokenStruct(uint256 _tokenNum) private view returns (Cero memory _token) {
        (uint16 _st, uint16 _pr, uint16 _ag, uint16 _ma, uint8 _lvl, bool _isChild) =
            tokenContract.getTokenShortDataType1(_tokenNum);

        _token = Cero({
            name: "",
            lvl: _lvl,
            strength: _st,
            protection: _pr,
            agility: _ag,
            magic: _ma,
            parent1: 0,
            parent2: 0,
            isChild: _isChild,
            birthday: 0,
            experience: 0
        });
    }
}
