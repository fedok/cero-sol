// SPDX-License-Identifier: MIT
pragma solidity ^0.7.2;

import "@openzeppelin/contracts/access/Ownable.sol";

/// @title This contract control contract balance
/// @author Oleksandr Fedorenko
contract BalanceManagement is Ownable {
    /// @notice Check contract balance.
    /// @return Contract balance
    function getBalance() external view onlyOwner returns (uint256) {
        return address(this).balance;
    }

    /// @notice Withdraw reward from contract.
    function withdrawReward() external onlyOwner {
        address payable owner = payable(owner());
        owner.transfer(address(this).balance);
    }
}
