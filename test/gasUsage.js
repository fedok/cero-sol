const { use } = require('chai');
const { solidity } = require('ethereum-waffle');

const CeroToken = artifacts.require('CeroToken');
const CreateBaseCero = artifacts.require('CreateBaseCero');
const CreateChildCero = artifacts.require('CreateChildCero');
const LevelUp = artifacts.require('LevelUp');
const Fight = artifacts.require('Fight');

const Reverter = require('./helpers/reverter');
const Helper = require('./helpers/helper');

use(solidity);
use(require('chai-as-promised')).should();

contract('Transaction statistic', (accounts) => {
  const reverter = new Reverter(web3);
  const h = new Helper();

  const OLEG = accounts[1];
  const IGOR = accounts[2];

  const gasUsageResults = [];
  const gasPrice = 200;
  const ethPrice = 2500;

  let ceroToken;
  let createBaseCero;
  let createChildCero;
  let levelUp;
  let fight;
  let fee;

  before(async () => {
    ceroToken = await CeroToken.new();
    createBaseCero = await CreateBaseCero.new(ceroToken.address);
    createChildCero = await CreateChildCero.new(ceroToken.address);
    levelUp = await LevelUp.new(ceroToken.address);
    fight = await Fight.new(ceroToken.address, createBaseCero.address);

    await ceroToken.generateFirstToken();
    fee = (await ceroToken.fee()).toString();

    await ceroToken.addOwnership(createBaseCero.address);
    await ceroToken.addOwnership(createChildCero.address);
    await ceroToken.addOwnership(levelUp.address);
    await ceroToken.addOwnership(fight.address);

    // Get deployed gas usage
    const receipt1 = await web3.eth.getTransactionReceipt(ceroToken.transactionHash);
    gasUsageResults.push({ name: 'CeroToken - deploy', gasUsage: receipt1.gasUsed });

    const receipt2 = await web3.eth.getTransactionReceipt(createBaseCero.transactionHash);
    gasUsageResults.push({ name: 'CreateBaseCero - deploy', gasUsage: receipt2.gasUsed });

    const receipt3 = await web3.eth.getTransactionReceipt(createChildCero.transactionHash);
    gasUsageResults.push({ name: 'CreateChildCero - deploy', gasUsage: receipt3.gasUsed });

    const receipt4 = await web3.eth.getTransactionReceipt(levelUp.transactionHash);
    gasUsageResults.push({ name: 'LevelUp - deploy', gasUsage: receipt4.gasUsed });

    const receipt5 = await web3.eth.getTransactionReceipt(fight.transactionHash);
    gasUsageResults.push({ name: 'Fight - deploy', gasUsage: receipt5.gasUsed });

    await reverter.snapshot();
  });

  afterEach(async () => {
    await reverter.revert();
  });

  describe('Form transaction statistic', async () => {
    it('createBaseCero()', async () => {
      await createBaseCero.createBaseCero(OLEG, { from: OLEG, value: fee }).then((res) => {
        gasUsageResults.push({ name: 'createBaseCero()', gasUsage: res.receipt.gasUsed });
      });
    });

    it('createChildCero()', async () => {
      await createBaseCero.createBaseCero(OLEG, { value: fee }); // Token 1
      await createBaseCero.createBaseCero(OLEG, { value: fee }); // Token 2

      await createChildCero.createChildCero(1, 2, { from: OLEG, value: fee }).then((res) => {
        gasUsageResults.push({ name: 'createChildCero()', gasUsage: res.receipt.gasUsed });
      });
    });

    it('levelUp()', async () => {
      await createBaseCero.createBaseCero(OLEG, { value: fee }); // Token 1
      await createBaseCero.createBaseCero(IGOR, { value: fee }); // Token 2

      for (let i = 0; i < 3; i++) {
        await fight.fight(1, { value: fee });
      }

      await levelUp.levelUp(1, 3, 3, 3, 4, { from: OLEG, value: fee }).then((res) => {
        gasUsageResults.push({ name: 'levelUp()', gasUsage: res.receipt.gasUsed });
      });
    });

    it('setName1()', async () => {
      await ceroToken.setName1('New name 15').then((res) => {
        gasUsageResults.push({ name: 'setName()', gasUsage: res.receipt.gasUsed });
      });
    });

    it('transferFrom()', async () => {
      await createBaseCero.createBaseCero(OLEG, { value: fee }); // Token 1

      await ceroToken.approve(IGOR, 1, { from: OLEG }).then((res) => {
        gasUsageResults.push({ name: 'approve()', gasUsage: res.receipt.gasUsed });
      });

      await ceroToken.transferFrom(OLEG, IGOR, 1, { from: OLEG }).then((res) => {
        gasUsageResults.push({ name: 'transferFrom()', gasUsage: res.receipt.gasUsed });
      });
    });

    it('fight()', async () => {
      await createBaseCero.createBaseCero(IGOR, { value: fee }); // Token 1

      for (let i = 0; i <= 50; i++) {
        await createBaseCero.createBaseCero(OLEG, { value: fee }); // Token 2...
        if (i % 10 === 0) {
          await fight.fight(2, { value: fee }).then((res) => {
            gasUsageResults.push({ name: `fight() on iteration ${i}`, gasUsage: res.receipt.gasUsed });
          });
        }
      }
    });
  });

  describe('form result', async () => {
    it('form result', async () => {
      console.log('EthPrice - ', ethPrice);
      console.log('GasPrice - ', gasPrice);
      console.log(h.createRow(['Name', 'Gas usage', 'ETH', 'USD'], 30));
      gasUsageResults.forEach((obj) => {
        const TransactionETH = (gasPrice * obj.gasUsage) / 10 ** 9;
        const TransactionUSD = TransactionETH * ethPrice;
        console.log(h.createRow([obj.name, obj.gasUsage, TransactionETH, TransactionUSD], 30));
      });
    });
  });
});
