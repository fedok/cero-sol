// SPDX-License-Identifier: MIT
pragma solidity ^0.7.2;

/// @title This contract generate token name and control arrays with name.
/// @author Oleksandr Fedorenko
contract Randomizer {
    /// @notice Get conventionally random number.
    /// @param _max Maximal value
    /// @return _result Number
    function random(uint256 _max) internal view returns (uint256 _result) {
        _result = uint256(keccak256(abi.encodePacked(block.number, block.difficulty, block.timestamp))) % _max;
    }

    /// @notice Get conventionally random number with more entropy.
    /// @param _max Maximal value
    /// @param _max Maximal value
    /// @return _result Number
    function random(uint256 _max, uint256 _randomCounter) internal view returns (uint256 _result) {
        _result =
            uint256(keccak256(abi.encodePacked(block.number, block.difficulty, block.timestamp, _randomCounter))) %
            _max;
    }
}
