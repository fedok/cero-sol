const { use, expect } = require('chai');
const { solidity } = require('ethereum-waffle');
const truffleAssert = require('truffle-assertions');

const CeroToken = artifacts.require('CeroToken');
const CreateBaseCero = artifacts.require('CreateBaseCero');
const CreateChildCero = artifacts.require('CreateChildCero');

const Reverter = require('./helpers/reverter');
const Helper = require('./helpers/helper');

use(solidity);
use(require('chai-as-promised')).should();

contract('CreateChildCero', (accounts) => {
  const reverter = new Reverter(web3);
  const h = new Helper();

  const OWNER = accounts[0];
  const OLEG = accounts[1];
  const IVAN = accounts[2];

  let ceroToken;
  let createBaseCero;
  let createChildCero;
  let fee;

  before(async () => {
    ceroToken = await CeroToken.new();
    createBaseCero = await CreateBaseCero.new(ceroToken.address);
    createChildCero = await CreateChildCero.new(ceroToken.address);

    await ceroToken.generateFirstToken();
    fee = (await ceroToken.fee()).toString();

    await ceroToken.addOwnership(createBaseCero.address);
    await ceroToken.addOwnership(createChildCero.address);

    await reverter.snapshot();
  });
  afterEach(async () => {
    await reverter.revert();
  });

  describe('createChildCero()', async () => {
    beforeEach(async () => {
      await createBaseCero.createBaseCero(IVAN, { value: fee }); // Token 1
      await createBaseCero.createBaseCero(OLEG, { value: fee }); // Token 2
      await createBaseCero.createBaseCero(OLEG, { value: fee }); // Token 3
      await createBaseCero.createBaseCero(OLEG, { value: fee }); // Token 4
      await createBaseCero.createBaseCero(OLEG, { value: fee }); // Token 5
    });

    it('check creation result ', async () => {
      const tokenParent1 = 2;
      const tokenParent2 = 3;
      await createChildCero.createChildCero(tokenParent1, tokenParent2, { from: OLEG, value: fee }); // Token 6

      let child = await ceroToken.ceroes(6);
      child = await h.solCeroToJSCero(child);

      assert.equal(child.isChild, true);
      assert.equal(child.lvl, 2);
      assert.equal(child.parent1, tokenParent1);
      assert.equal(child.parent2, tokenParent2);
      assert.equal(child.experience, 13);

      let parent1 = await ceroToken.ceroes(tokenParent1);
      parent1 = await h.solCeroToJSCero(parent1);
      assert.equal(parent1.isChild, false);

      let parent2 = await ceroToken.ceroes(tokenParent2);
      parent2 = await h.solCeroToJSCero(parent2);
      assert.equal(parent2.isChild, false);
    });

    it('revert when tokens owner not a msg.sender', async () => {
      await expect(createChildCero.createChildCero(1, 2, { from: IVAN, value: fee }))
        .to.be.revertedWith('[ECCC-101] - You are not a token owner.');
      await expect(createChildCero.createChildCero(2, 1, { from: IVAN, value: fee }))
        .to.be.revertedWith('[ECCC-101] - You are not a token owner.');
    });

    it('revert on different lvl and when token have child', async () => {
      await createBaseCero.createBaseCero(OLEG, { from: OWNER, value: fee }); // Token 6

      await createChildCero.createChildCero(2, 3, { from: OLEG, value: fee }); // Token 7
      await createChildCero.createChildCero(4, 5, { from: OLEG, value: fee }); // Token 8

      await expect(createChildCero.createChildCero(5, 7, { from: OLEG, value: fee }))
        .to.be.revertedWith('[ECCC-102] - Must be on the same level.');

      await expect(createChildCero.createChildCero(6, 2, { from: OLEG, value: fee }))
        .to.be.revertedWith('[ECCC-105] - There should be no descendants.');
    });

    it('should revert if not enough funds', async () => {
      await expect(createChildCero.createChildCero(2, 3, { from: OLEG }))
        .to.be.revertedWith('[ECCC-111] - Insufficient funds.');
    });

    it('should revert if same tokens', async () => {
      await expect(createChildCero.createChildCero(2, 2, { from: OLEG, value: fee }))
        .to.be.revertedWith('[ECCC-106] - The numbers must not match.');
    });

    it('should catch event ChildCreate', async () => {
      const resp = await createChildCero.createChildCero(2, 3, { from: OLEG, value: fee });
      truffleAssert.eventEmitted(resp, 'ChildCreate', async (event) => {
        assert.equal(event.newChild.toString(), '6');
        assert.equal(event.parent1.toString(), '2');
        assert.equal(event.parent2.toString(), '3');
        return true;
      });
    });
  });
});
