// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.7.2;

interface ICeroStruct {
    /// @notice Main token structure.
    /// @param name Token name
    /// @param lvl Token level
    /// @param strength Token strength parameter
    /// @param protection Token protection parameter
    /// @param agility Token agility parameter
    /// @param magic Token magic parameter
    /// @param parent1 Token parent after merge
    /// @param parent2 Token parent after merge
    /// @param isChild False if token have a child
    /// @param birthday Token creation time
    /// @param experience Token experience
    struct Cero {
        uint8 lvl;
        uint16 strength;
        uint16 protection;
        uint16 agility;
        uint16 magic;
        uint64 birthday;
        uint256 parent1;
        uint256 parent2;
        uint256 experience;
        string name;
        bool isChild;
    }
}
