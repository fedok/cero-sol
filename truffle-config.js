const HDWalletProvider = require('truffle-hdwallet-provider');
require('dotenv').config(); // Store environment-specific variable from '.env' to process.env

module.exports = {
  networks: {
    development: {
      host: 'localhost', // Localhost (default: none)
      port: 8545, // Standard Ethereum port (default: none)
      network_id: '*', // Any network (default: none)
      gas: 6000000,
      gasLimit: 6000000, // <-- Use this high gas value
      gasPrice: 1,
    },
    ropsten: {
      provider: () => new HDWalletProvider(
        process.env.MNENOMIC,
        `https://ropsten.infura.io/v3/${process.env.INFURA_API_KEY}`,
      ),
      network_id: 3,
      // gas: 8000000,
      // gasLimit: 8000000,
      gasPrice: 15000000000,
    },
    rinkeby: {
      provider: () => new HDWalletProvider(
        process.env.MNENOMIC,
        `https://rinkeby.infura.io/v3/${process.env.INFURA_API_KEY}`,
      ),
      network_id: 4,
      gasPrice: 5000000000,
    },
    kovan: {
      provider: () => new HDWalletProvider(
        process.env.MNENOMIC,
        `https://kovan.infura.io/v3/${process.env.INFURA_API_KEY}`, 0, 3,
      ),
      network_id: 42,
      // gas: 8000000,
      // gasLimit: 8000000,
      gasPrice: 10000000,
    },
    main: {
      provider: () => new HDWalletProvider(
        process.env.MNENOMIC, `https://mainnet.infura.io/v3/${process.env.INFURA_API_KEY}`,
      ),
      gasPrice: 1,
      network_id: 1,
    },
    velasDevNet: {
      provider: () => new HDWalletProvider(
        process.env.MNENOMIC, 'https://explorer.testnet.velas.com/rpc',
      ),
      network_id: 111,
      networkCheckTimeout: 120000,
      timeoutBlocks: 200,
      skipDryRun: true,
    },
  },
  mocha: {
    // timeout: 100000
  },
  compilers: {
    solc: {
      version: '0.7.2',
      settings: {
        optimizer: {
          enabled: true,
          runs: 1000,
        },
      },
    },
  },
  api_keys: {
    etherscan: process.env.ETHERSCAN_API_KEY,
  },
  plugins: [
    'truffle-plugin-solhint',
    'truffle-plugin-verify',
  ],
};
