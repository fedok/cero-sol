qa:
	@npm run prettier:solidity && truffle run solhint

start-ganache-cli:
	ganache-cli --account="0xe6c0930c55083f1210ecd02bb3864a7f9d7023e93c51cc87cc16c35c388770b5, 100000000000000000000 " \
		--account="0xc8cc2f98b894f18a4a52fc136fd96a8b31149afa00801fbde611735abf9cf79e, 90000000000000000000" \
		--account="0xebd57f63afaae4326842f01f76b10560162f00adc31b619f45ad8baa200a92ec, 80000000000000000000"

verify:
	truffle run verify CreateChildCero@0x5C80Ddd187054E1E4aBBfFCD750498e81d34FfA3 --network ropsten && \
	truffle run verify LevelUp@0x492aC8072c82d4FDDbba2FC119eC1Efd9796E268 --network ropsten && \
	truffle run verify Fight@0x362cc346fbdb79636260eA149954104048E74785 --network ropsten
