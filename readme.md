#### --- Cero game ---

**Developer**: Oleksandr Fedorenko

**Email**: agoodminute@gmail.com

**Telegram**: @agoodminute

#### --- Commands ---

`truffle compile --all` - compile contracts

`truffle test` - run tests

`truffle exec deploy.js --network [name]` - deploy contracts to network

`truffle run solhint` - run solhint

`npm run prettier:solidity` - run prettier

 `truffle run verify <contract_name>@<contract_address> --network ropsten` - verify contract
