const { use, expect } = require('chai');
const { solidity } = require('ethereum-waffle');
const truffleAssert = require('truffle-assertions');

const CeroToken = artifacts.require('CeroToken');
const CreateBaseCero = artifacts.require('CreateBaseCero');
const CreateChildCero = artifacts.require('CreateChildCero');
const LevelUp = artifacts.require('LevelUp');
const Fight = artifacts.require('Fight');

const Reverter = require('./helpers/reverter');
const Helper = require('./helpers/helper');

use(solidity);
use(require('chai-as-promised')).should();

contract('Fight', (accounts) => {
  const reverter = new Reverter(web3);
  const h = new Helper();

  const OWNER = accounts[0];
  const OLEG = accounts[1];
  const IVAN = accounts[2];

  let ceroToken;
  let createBaseCero;
  let createChildCero;
  let levelUp;
  let fight;
  let fee;

  before(async () => {
    ceroToken = await CeroToken.new();
    createBaseCero = await CreateBaseCero.new(ceroToken.address);
    createChildCero = await CreateChildCero.new(ceroToken.address);
    levelUp = await LevelUp.new(ceroToken.address);
    fight = await Fight.new(ceroToken.address, createBaseCero.address);

    await ceroToken.generateFirstToken();
    fee = (await ceroToken.fee()).toString();

    await ceroToken.addOwnership(createBaseCero.address);
    await ceroToken.addOwnership(createChildCero.address);
    await ceroToken.addOwnership(levelUp.address);
    await ceroToken.addOwnership(fight.address);

    await reverter.snapshot();
  });
  afterEach(async () => {
    await reverter.revert();
  });

  describe('fight process', async () => {
    it('should catch event FightResult', async () => {
      await createBaseCero.createBaseCero(OLEG, { value: fee }); // Token 1
      await createBaseCero.createBaseCero(IVAN, { value: fee }); // Token 2

      const fightResp = await fight.fight(1, { value: fee });
      truffleAssert.eventEmitted(fightResp, 'FightResult', async (event) => {
        assert.equal(event.attacker.toNumber(), 1);
        assert.equal(event.defender.toNumber(), 2);
        assert.oneOf(event.winner.toNumber(), [1, 2]);
        assert.oneOf(event.isNewTokenAccepted, [true, false]);
        assert.isAtLeast(event.time.toNumber(), 1615036000);
        return true;
      });
    });

    it('should add 5 experience for winner and 2 for looser and no more than max exp for lvl', async () => {
      await createBaseCero.createBaseCero(OLEG, { value: fee }); // Token 1
      await createBaseCero.createBaseCero(IVAN, { value: fee }); // Token 2

      let lvl = 1;
      let maxExp = h.getExperienceCountForLevelUp(lvl + 1);
      const expPerWin = 5;
      const expPerLoose = 2;

      let tok1BaseExp = 8;
      let tok2BaseExp = 8;

      for (let i = 0; i < 20; i++) {
        await fight.fight(1, { value: fee });
        const log = await fight.getLogsForToken(1);

        const tok1Exp = h.solCeroToJSCero(await ceroToken.ceroes(1)).experience;
        const tok2Exp = h.solCeroToJSCero(await ceroToken.ceroes(2)).experience;

        if (log[i].winner === log[i].attacker) {
          tok1BaseExp = tok1BaseExp + expPerWin > maxExp ? maxExp : tok1BaseExp + expPerWin;
          tok2BaseExp = tok2BaseExp + expPerLoose > maxExp ? maxExp : tok2BaseExp + expPerLoose;

          assert.equal(tok1Exp, tok1BaseExp);
          assert.equal(tok2Exp, tok2BaseExp);
        } else {
          tok1BaseExp = tok1BaseExp + expPerLoose > maxExp ? maxExp : tok1BaseExp + expPerLoose;
          tok2BaseExp = tok2BaseExp + expPerWin > maxExp ? maxExp : tok2BaseExp + expPerWin;

          assert.equal(tok1Exp, tok1BaseExp);
          assert.equal(tok2Exp, tok2BaseExp);
        }

        if (tok1Exp === maxExp && tok2Exp === maxExp) {
          await levelUp.levelUp(1, 3, 3, 3, 4, { from: OLEG, value: fee });
          await levelUp.levelUp(2, 3, 3, 3, 4, { from: IVAN, value: fee });
          lvl += 1;
          maxExp = h.getExperienceCountForLevelUp(lvl + 1);
        }
      }
    });

    it('should revert if fighter have descendants', async () => {
      // Create child
      await createBaseCero.createBaseCero(OLEG, { from: OWNER, value: fee }); // Token 1
      await createBaseCero.createBaseCero(OLEG, { from: OWNER, value: fee }); // Token 2

      await createChildCero.createChildCero(1, 2, { from: OLEG, value: fee }); // Token 3

      await expect(fight.fight(1, { value: fee })).to.be.revertedWith('[EF-602] - There should be no descendants.');
      await expect(fight.fight(2, { value: fee })).to.be.revertedWith('[EF-602] - There should be no descendants.');
    });

    it('should revert opponent not found when only descendants and more level defender', async () => {
      // Create child
      await createBaseCero.createBaseCero(OLEG, { from: OWNER, value: fee }); // Token 1. Lvl 1. Have child
      await createBaseCero.createBaseCero(OLEG, { from: OWNER, value: fee }); // Token 2. Lvl 1. Have child

      await createChildCero.createChildCero(1, 2, { from: OLEG, value: fee }); // Token 3. Lvl 2

      await createBaseCero.createBaseCero(IVAN, { from: OWNER, value: fee }); // Token 4. Lvl 1
      await expect(fight.fight(4, { value: fee })).to.be.revertedWith('[EF-603] - Opponent not found.');
    });

    it('should fight many times and write acceptedToCreateBaseToken for winner', async () => {
      for (let i = 0; i < 10; i++) {
        if (i % 2 === 0) {
          await createBaseCero.createBaseCero(OLEG, { value: fee });
        } else {
          await createBaseCero.createBaseCero(IVAN, { value: fee });
        }
      }

      let newTokenOleg = 0;
      let newTokenIvan = 0;

      for (let i = 1; i <= 10; i++) {
        const fightResp = await fight.fight(i, { value: fee });
        truffleAssert.eventEmitted(fightResp, 'FightResult', (event) => {
          if (event.isNewTokenAccepted === true) {
            if (i % 2 === 0) newTokenIvan++;
            else newTokenOleg++;
          }
          return true;
        });
      }

      const newTokenOlegFromBlockchain = await ceroToken.acceptedToCreateBaseToken(OLEG);
      assert.equal(newTokenOlegFromBlockchain.toNumber(), newTokenOleg);
      const newTokenIvanFromBlockchain = await ceroToken.acceptedToCreateBaseToken(IVAN);
      assert.equal(newTokenIvanFromBlockchain.toNumber(), newTokenIvan);
    });

    it('should revert if not enough funds', async () => {
      await createBaseCero.createBaseCero(OLEG, { value: fee }); // Token 1
      await createBaseCero.createBaseCero(IVAN, { value: fee }); // Token 2
      await expect(fight.fight(1)).to.be.revertedWith('[EF-601] - Insufficient funds.');
    });
  });
});
