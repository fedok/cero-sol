// SPDX-License-Identifier: MIT
pragma solidity ^0.7.2;
pragma experimental ABIEncoderV2;

import "./services/Randomizer.sol";
import "./CeroToken.sol";
import "./services/BalanceManagement.sol";

/// @title This contract set base parameters for new tokens and send them to CeroERC721 contract.
/// @author Oleksandr Fedorenko
contract CreateBaseCero is Randomizer, BalanceManagement {
    /// @notice Contract address which contain tokens and tokens data.
    CeroToken public tokenContract;

    constructor(address _tokenContract) {
        tokenContract = CeroToken(_tokenContract);
    }

    /// @notice Create lvl 1 token. You can call this function only once free or with acceptance. Owner can call it any time.
    /// @param _tokenOwner New token owner address
    function createBaseCero(address _tokenOwner) external {
        if (!tokenContract.isOwner(msg.sender)) {
            uint256 _acceptedToCreateBaseToken = tokenContract.acceptedToCreateBaseToken(_tokenOwner);

            // If user already have tokens, check acceptance for creation new tokens
            if (tokenContract.balanceOf(_tokenOwner) > 0) {
                require(_acceptedToCreateBaseToken > 0, "[ECF-102] - This address can't create a new token.");
                tokenContract.setAcceptedToCreateBaseToken(_tokenOwner, _acceptedToCreateBaseToken - 1);
            }
        }

        uint256 _ceroType = random(3);
        if (_ceroType == 0) {
            _createBaseCero(_tokenOwner, 6, 6, 2, 0);
        } else if (_ceroType == 1) {
            _createBaseCero(_tokenOwner, 6, 2, 6, 0);
        } else if (_ceroType == 2) {
            _createBaseCero(_tokenOwner, 0, 2, 6, 6);
        }
    }

    /// @param _tokenOwner New token owner address.
    /// @param _stB Base token strenght
    /// @param _prB Base token protection
    /// @param _agB Base token agility
    /// @param _maB Base token magic
    function _createBaseCero(
        address _tokenOwner,
        uint16 _stB,
        uint16 _prB,
        uint16 _agB,
        uint16 _maB
    ) private {
        uint256 bn = block.number;
        uint16 st = uint16(_stB + random(5, bn + 1));
        uint16 pr = uint16(_prB + random(5, bn + 2));
        uint16 ag = uint16(_agB + random(5, bn + 3));
        uint16 ma = uint16(_maB + random(5, bn + 4));

        st + pr > ag + ma || st + ag > ag + ma ? ma = 0 : st = 0;

        tokenContract.createToken(_tokenOwner, 1, st, pr, ag, ma, 0, 0, true);
    }
}
