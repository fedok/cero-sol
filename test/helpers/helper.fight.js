const Helper = require('./helper');

class HelperFight {
  constructor() {
    this.h = new Helper();
  }

  async formFightStats(ceroToken, fightResult) {
    console.log('----------------');

    let warVsTh = 0;
    let warVsThWarWin = 0;
    let warVsMag = 0;
    let warVsMagWarWin = 0;
    let thVsMag = 0;
    let thVsMagThWin = 0;

    for (const res of fightResult) {
      let attacker = await ceroToken.ceroes(res.attacker);
      let defender = await ceroToken.ceroes(res.defender);
      attacker = await this.h.solCeroToJSCero(attacker);
      defender = await this.h.solCeroToJSCero(defender);

      // Warrior VS Thief
      if (attacker.type === this.h.warrior && defender.type === this.h.thief) {
        warVsTh++;
        if (res.winner.toNumber() === res.attacker.toNumber()) {
          warVsThWarWin++;
        }
      }
      if (attacker.type === this.h.thief && defender.type === this.h.warrior) {
        warVsTh++;
        if (res.winner.toNumber() === res.defender.toNumber()) {
          warVsThWarWin++;
        }
      }

      // Warrior VS Magic
      if (attacker.type === this.h.warrior && defender.type === this.h.magic) {
        warVsMag++;
        if (res.winner.toNumber() === res.attacker.toNumber()) {
          warVsMagWarWin++;
        }
      }
      if (attacker.type === this.h.magic && defender.type === this.h.warrior) {
        warVsMag++;
        if (res.winner.toNumber() === res.defender.toNumber()) {
          warVsMagWarWin++;
        }
      }

      // Thief VS Magic
      if (attacker.type === this.h.thief && defender.type === this.h.magic) {
        thVsMag++;
        if (res.winner.toNumber() === res.attacker.toNumber()) {
          thVsMagThWin++;
        }
      }
      if (attacker.type === this.h.magic && defender.type === this.h.thief) {
        thVsMag++;
        if (res.winner.toNumber() === res.defender.toNumber()) {
          thVsMagThWin++;
        }
      }
    }

    const warVsThWarWinPer = warVsTh !== 0 ? this.h.calcPercent(warVsTh, warVsThWarWin) : 0;
    const warVsMagWarWinPer = warVsMag !== 0 ? this.h.calcPercent(warVsMag, warVsMagWarWin) : 0;
    const thVsMagThWinPer = thVsMag !== 0 ? this.h.calcPercent(thVsMag, thVsMagThWin) : 0;

    console.log(this.h.createRow(
      ['Fight', 'Total fights', 'Warrior win, %', 'Thief win, %', 'Magic win, %'], 18,
    ));
    console.log(this.h.createRow(
      ['Warrior vs Thief', warVsTh, warVsThWarWinPer, this.h.round(100 - warVsThWarWinPer), '-'], 18,
    ));
    console.log(this.h.createRow(
      ['Warrior vs Magic', warVsMag, warVsMagWarWinPer, '-', this.h.round(100 - warVsMagWarWinPer)], 18,
    ));
    console.log(this.h.createRow(
      ['Thief vs Magic', thVsMag, '-', thVsMagThWinPer, this.h.round(100 - thVsMagThWinPer)], 18,
    ));
  }

  // eslint-disable-next-line class-methods-use-this
  formFightResultInfo(fightResult) {
    console.log('----------------');
    console.log(this.h.createRow(['Token 1', 'Token 2', 'Winner'], 9));
    fightResult.forEach((res) => {
      console.log(this.h.createRow([res.attacker.toNumber(), res.defender.toNumber(), res.winner.toNumber()], 9));
    });
  }
}

module.exports = HelperFight;
