const { use, expect } = require('chai');
const { solidity } = require('ethereum-waffle');

const CeroToken = artifacts.require('CeroToken');
const CreateBaseCero = artifacts.require('CreateBaseCero');
const Fight = artifacts.require('Fight');

const Reverter = require('./helpers/reverter');

use(solidity);
use(require('chai-as-promised')).should();

contract('CreateBaseCero', (accounts) => {
  const reverter = new Reverter(web3);

  const OLEG = accounts[1];
  const IVAN = accounts[2];

  let ceroToken;
  let createBaseCero;
  let fight;
  let fee;

  before(async () => {
    ceroToken = await CeroToken.new();
    createBaseCero = await CreateBaseCero.new(ceroToken.address);
    fight = await Fight.new(ceroToken.address, createBaseCero.address);

    await ceroToken.generateFirstToken();
    fee = (await ceroToken.fee()).toString();

    await ceroToken.addOwnership(createBaseCero.address);
    await ceroToken.addOwnership(fight.address);

    await reverter.snapshot();
  });

  afterEach(async () => {
    await reverter.revert();
  });

  describe('createBaseCero()', async () => {
    beforeEach(async () => {
      await createBaseCero.createBaseCero(OLEG, { from: OLEG, value: fee }); // Token 1
      await createBaseCero.createBaseCero(IVAN, { from: IVAN, value: fee }); // Token 2
    });

    it('should return right token owner', async () => {
      (await ceroToken.ownerOf(1)).should.be.equal(OLEG);
      (await ceroToken.ownerOf(2)).should.be.equal(IVAN);
    });

    it('should revert when try to create first token again', async () => {
      await expect(createBaseCero.createBaseCero(OLEG, { from: OLEG, value: fee }))
        .to.be.revertedWith("[ECF-102] - This address can't create a new token.");
      await expect(createBaseCero.createBaseCero(IVAN, { from: IVAN, value: fee }))
        .to.be.revertedWith("[ECF-102] - This address can't create a new token.");
    });

    it('should revert when try to create first token again', async () => {
      let newTokenOlegFromBlockchain = 0;
      while (newTokenOlegFromBlockchain === 0) {
        await fight.fight(1, { value: fee });
        newTokenOlegFromBlockchain = (await ceroToken.acceptedToCreateBaseToken(OLEG)).toNumber();
      }

      await createBaseCero.createBaseCero(OLEG, { from: OLEG, value: fee });
      await expect(createBaseCero.createBaseCero(OLEG, { from: OLEG, value: fee }))
        .to.be.revertedWith("[ECF-102] - This address can't create a new token.");
      (await ceroToken.acceptedToCreateBaseToken(OLEG)).toNumber().should.be.equal(0);
    });

    it('should always create token when caller is contract owner', async () => {
      await createBaseCero.createBaseCero(OLEG, { value: fee });
      await createBaseCero.createBaseCero(OLEG, { value: fee });
    });

    it('should revert if not enough funds', async () => {
      await expect(createBaseCero.createBaseCero(OLEG))
        .to.be.revertedWith('[ECF-103] - Insufficient funds.');
    });
  });
});
