// SPDX-License-Identifier: MIT
pragma solidity ^0.7.2;
pragma experimental ABIEncoderV2;

import "./interfaces/ICeroStruct.sol";
import "./CeroToken.sol";
import "./services/Globals.sol";
import "./services/BalanceManagement.sol";

/// @title This contract upgrade tokens.
/// @author Oleksandr Fedorenko
contract LevelUp is ICeroStruct, Globals, BalanceManagement {
    /// @notice Contract address which contain tokens and tokens data.
    CeroToken public tokenContract;

    constructor(address _tokenContract) {
        tokenContract = CeroToken(_tokenContract);
    }

    /// @notice Points that can be distribute between parameters.
    uint8 public pointForDistribution = 13;

    /// @notice Upgrade token.
    /// @param _ceroNum Token number to upgrade
    /// @param _st Strength points that will be added to token
    /// @param _pr Protection points that will be added to token
    /// @param _ag Agility points that will be added to token
    /// @param _ma Magic points that will be added to token
    function levelUp(
        uint256 _ceroNum,
        uint16 _st,
        uint16 _pr,
        uint16 _ag,
        uint16 _ma
    ) external {
        require(msg.sender == tokenContract.ownerOf(_ceroNum), "[ELU-449] - Not a token owner.");
        require(
            _st + _pr + _ag + _ma == pointForDistribution,
            "[ELU-450] - The sum of points for distribution does not match."
        );

        Cero memory _cero = tokenDataToTokenStruct(_ceroNum);

        require(_cero.isChild, "[ELU-452] - There should be no descendants.");
        require(_cero.experience >= getExperienceCountForLevelUp(_cero.lvl + 1), "[ELU-451] - Not enough experience.");

        _cero.strength += _st;
        _cero.protection += _pr;
        _cero.agility += _ag;
        _cero.magic += _ma;
        _cero.lvl += 1;

        uint256 _wizardPoint = _cero.magic + _cero.agility;
        _cero.strength + _cero.protection > _wizardPoint || _cero.strength + _cero.agility > _wizardPoint
            ? _cero.magic = 0
            : _cero.strength = 0;

        tokenContract.updateTokenAfterLevelUp(
            _ceroNum,
            _cero.strength,
            _cero.protection,
            _cero.agility,
            _cero.magic,
            _cero.lvl
        );
    }

    /// @notice Transform data from token contract to memory struct.
    /// @param _tokenNum Token number
    /// @return _token Token data
    function tokenDataToTokenStruct(uint256 _tokenNum) private view returns (Cero memory _token) {
        (uint16 _st, uint16 _pr, uint16 _ag, uint16 _ma, uint8 _lvl, bool _isChild, uint256 _exp) =
            tokenContract.getTokenShortDataType2(_tokenNum);

        _token = Cero({
            name: "",
            lvl: _lvl,
            strength: _st,
            protection: _pr,
            agility: _ag,
            magic: _ma,
            parent1: 0,
            parent2: 0,
            isChild: _isChild,
            birthday: 0,
            experience: _exp
        });
    }
}
