const { use, expect } = require('chai');
const { solidity } = require('ethereum-waffle');

const CeroToken = artifacts.require('CeroToken');

const Reverter = require('./helpers/reverter');

use(solidity);
use(require('chai-as-promised')).should();

contract('BalanceManagement', (accounts) => {
  const reverter = new Reverter(web3);

  const OWNER = accounts[0];
  const OLEG = accounts[1];
  const IVAN = accounts[2];
  const ZERO = '0x0000000000000000000000000000000000000000';

  let ceroToken;

  before(async () => {
    ceroToken = await CeroToken.new();

    await reverter.snapshot();
  });

  afterEach(async () => {
    await reverter.revert();
  });

  describe('isOwner()', async () => {
    it('should return true for OWNER', async () => {
      assert.equal(await ceroToken.isOwner(OWNER), true);
    });

    it('should return false for not owners', async () => {
      assert.equal(await ceroToken.isOwner(OLEG), false);
      assert.equal(await ceroToken.isOwner(IVAN), false);
    });
  });

  describe('addOwnership()', async () => {
    it('should add new owner', async () => {
      assert.equal(await ceroToken.isOwner(OLEG), false);
      await ceroToken.addOwnership(OLEG);
      assert.equal(await ceroToken.isOwner(OWNER), true);
      assert.equal(await ceroToken.isOwner(OLEG), true);
    });

    it('should revert if caller is not owner', async () => {
      await expect(ceroToken.addOwnership(OLEG, { from: OLEG }))
        .to.be.revertedWith('[E-111] - Caller is not the owner.');
    });

    it('should revert if new owner is zero address', async () => {
      await expect(ceroToken.addOwnership(ZERO))
        .to.be.revertedWith('[E-112] - New owner is the zero address.');
    });
  });

  describe('removeOwnership()', async () => {
    it('should remove owner', async () => {
      assert.equal(await ceroToken.isOwner(OWNER), true);
      assert.equal(await ceroToken.isOwner(OLEG), false);
      await ceroToken.addOwnership(OLEG);
      assert.equal(await ceroToken.isOwner(OWNER), true);
      assert.equal(await ceroToken.isOwner(OLEG), true);
      await ceroToken.removeOwnership(OLEG);
      assert.equal(await ceroToken.isOwner(OWNER), true);
      assert.equal(await ceroToken.isOwner(OLEG), false);
    });
  });

  describe('transferOwnership()', async () => {
    it('should transfer ownership', async () => {
      assert.equal(await ceroToken.isOwner(OLEG), false);
      await ceroToken.transferOwnership(OWNER, OLEG);
      assert.equal(await ceroToken.isOwner(OWNER), false);
      assert.equal(await ceroToken.isOwner(OLEG), true);
    });

    it('should revert if caller is not owner', async () => {
      await expect(ceroToken.transferOwnership(OWNER, OLEG, { from: OLEG }))
        .to.be.revertedWith('[E-111] - Caller is not the owner.');
    });

    it('should revert if new owner is zero address', async () => {
      await expect(ceroToken.transferOwnership(OWNER, ZERO))
        .to.be.revertedWith('[E-113] - New owner is the zero address.');
    });
  });
});
