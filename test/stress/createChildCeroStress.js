const { use } = require('chai');
const { solidity } = require('ethereum-waffle');

const CeroToken = artifacts.require('CeroToken');
const CreateBaseCero = artifacts.require('CreateBaseCero');
const CreateChildCero = artifacts.require('CreateChildCero');

const Reverter = require('../helpers/reverter');
const Helper = require('../helpers/helper');
const HelperCreateChild = require('../helpers/helper.create-child');

use(solidity);
use(require('chai-as-promised')).should();

contract('createChildCero. Stress', (accounts) => {
  const reverter = new Reverter(web3);
  const h = new Helper();
  const hcc = new HelperCreateChild();
  const tokenLvl1 = 24;

  const OLEG = accounts[1];

  let ceroToken;
  let createBaseCero;
  let createChildCero;
  let fee;

  before(async () => {
    ceroToken = await CeroToken.new();
    createBaseCero = await CreateBaseCero.new(ceroToken.address);
    createChildCero = await CreateChildCero.new(ceroToken.address);

    await ceroToken.generateFirstToken();
    fee = (await ceroToken.fee()).toString();

    await ceroToken.addOwnership(createBaseCero.address);
    await ceroToken.addOwnership(createChildCero.address);

    await reverter.snapshot();
  });
  afterEach(async () => {
    await reverter.revert();
  });

  describe('CreateChildCero. Stress.', async () => {
    beforeEach(async () => {
      for (let i = 0; i < tokenLvl1; i++) {
        await createBaseCero.createBaseCero(OLEG, { value: fee });
      }
    });

    it('create lvl 2', async () => {
      let tokenCount = tokenLvl1;

      for (let i = 1; i < tokenLvl1; i += 2) {
        await createChildCero.createChildCero(i, i + 1, { from: OLEG, value: fee });
        tokenCount++;

        const parent1 = h.solCeroToJSCero(await ceroToken.ceroes(i));
        const parent2 = h.solCeroToJSCero(await ceroToken.ceroes(i + 1));
        const child = h.solCeroToJSCero(await ceroToken.ceroes(tokenCount));
        const toDist = hcc.calcCharacteristicToDistribution(parent1, parent2);

        assert.equal(toDist.strength + toDist.protection + toDist.agility + toDist.magic, 11);
        assert.oneOf(child.strength, [parent1.strength + toDist.strength, parent2.strength + toDist.strength]);
        assert.oneOf(child.protection,
          [parent1.protection + toDist.protection, parent2.protection + toDist.protection]);
        assert.oneOf(child.agility, [parent1.agility + toDist.agility, parent2.agility + toDist.agility]);
        assert.oneOf(child.magic, [parent1.magic + toDist.magic, parent2.magic + toDist.magic]);
      }

      (await ceroToken.balanceOf(OLEG)).toNumber().should.be.equal(tokenCount);
    });

    it('createChildCero create lvl 5', async () => {
      let tokenCount = tokenLvl1;
      let tokenLvl2 = 0;
      let tokenLvl3 = 0;
      let tokenLvl4 = 0;
      let tokenLvl5 = 0;

      // console.log(h.createRow(['#', 'Stat_1', 'Stat_2', 'Distrib', 'Result']));

      for (let i = 1; i < tokenLvl1; i += 2) {
        await createChildCero.createChildCero(i, i + 1, { from: OLEG, value: fee });
        tokenCount++;
        tokenLvl2++;
      }
      for (let i = tokenLvl1 + 1; i < tokenLvl1 + tokenLvl2; i += 2) {
        await createChildCero.createChildCero(i, i + 1, { from: OLEG, value: fee });
        tokenCount++;
        tokenLvl3++;
      }
      for (let i = tokenLvl1 + tokenLvl2 + 1; i < tokenLvl1 + tokenLvl2 + tokenLvl3; i += 2) {
        await createChildCero.createChildCero(i, i + 1, { from: OLEG, value: fee });
        tokenCount++;
        tokenLvl4++;
      }

      for (let i = tokenLvl1 + tokenLvl2 + tokenLvl3 + 1; i < tokenLvl1 + tokenLvl2 + tokenLvl3 + tokenLvl4; i += 2) {
        await createChildCero.createChildCero(i, i + 1, { from: OLEG, value: fee });
        tokenCount++;
        tokenLvl5++;

        const cero1 = h.solCeroToJSCero(await ceroToken.ceroes(i));
        const cero2 = h.solCeroToJSCero(await ceroToken.ceroes(i + 1));
        const cero3 = h.solCeroToJSCero(await ceroToken.ceroes(tokenCount));

        const toDist = hcc.calcCharacteristicToDistribution(cero1, cero2);

        // console.log(h.createRow(['St', cero1.strength, cero2.strength, toDist.strength, cero3.strength]));
        // console.log(h.createRow(['Pr', cero1.protection, cero2.protection, toDist.protection, cero3.protection]));
        // console.log(h.createRow(['Ag', cero1.agility, cero2.agility, toDist.agility, cero3.agility]));
        // console.log(h.createRow(['Ma', cero1.magic, cero2.magic, toDist.magic, cero3.magic]));
        // console.log(h.createRow(['-', '-', '-', '-', '-']));

        assert.equal(toDist.strength + toDist.protection + toDist.agility + toDist.magic, 11);
        assert.oneOf(cero3.strength, [cero1.strength + toDist.strength, cero2.strength + toDist.strength]);
        assert.oneOf(cero3.protection, [cero1.protection + toDist.protection, cero2.protection + toDist.protection]);
        assert.oneOf(cero3.agility, [cero1.agility + toDist.agility, cero2.agility + toDist.agility]);
        assert.oneOf(cero3.magic, [cero1.magic + toDist.magic, cero2.magic + toDist.magic]);
      }

      (await ceroToken.balanceOf(OLEG)).toNumber()
        .should.be.equal(tokenLvl1 + tokenLvl2 + tokenLvl3 + tokenLvl4 + tokenLvl5);
    });
  });
});
