class HelperCreateChild {
  constructor() {
    this.pointPerLevel = 11;
  }

  calcCharacteristicToDistribution(cero1, cero2) {
    const parent1 = {
      strength: cero1.strength,
      protection: cero1.protection,
      agility: cero1.agility,
      magic: cero1.magic,
    };
    const parent2 = {
      strength: cero2.strength,
      protection: cero2.protection,
      agility: cero2.agility,
      magic: cero2.magic,
    };
    const child = {
      strength: 0,
      protection: 0,
      agility: 0,
      magic: 0,
    };

    // Get sum of characteristic
    child.strength = parent1.strength + parent2.strength;
    child.protection = parent1.protection + parent2.protection;
    child.agility = parent1.agility + parent2.agility;
    child.magic = parent1.magic + parent2.magic;
    // console.log(cero3);

    // Reset to zero strength ot magic
    const warPoint = child.strength + child.protection;
    const thiPoint = child.strength + child.agility;
    const magPoint = child.magic + child.agility;
    if (warPoint > magPoint || thiPoint > magPoint) {
      child.magic = 0;
    } else {
      child.strength = 0;
    }
    // console.log('Characteristic sum:', cero3);

    // Get point sum
    const pointSum = child.strength + child.protection + child.agility + child.magic;

    // Get new point for distribution
    child.strength = Math.floor((child.strength * this.pointPerLevel) / pointSum);
    child.protection = Math.floor((child.protection * this.pointPerLevel) / pointSum);
    child.agility = Math.floor((child.agility * this.pointPerLevel) / pointSum);
    child.magic = Math.floor((child.magic * this.pointPerLevel) / pointSum);
    // console.log('Point ot distribution:', child);

    // Get point sum for distribution
    const pointLooses = this.pointPerLevel - (child.strength + child.protection + child.agility + child.magic);

    const maxChar = Math.max(child.strength, child.protection, child.agility, child.magic);

    if (child.strength === maxChar) {
      child.strength += pointLooses;
    } else if (child.protection === maxChar) {
      child.protection += pointLooses;
    } else if (child.agility === maxChar) {
      child.agility += pointLooses;
    } else if (child.magic === maxChar) {
      child.magic += pointLooses;
    }

    // console.log('Point after looses dist:', child);
    // console.log('parent1:', parent1);
    // console.log('parent2:', parent2);

    return child;
  }
}

module.exports = HelperCreateChild;
