const Helper = require('./helper');

class HelperCreateFirst {
  constructor() {
    this.h = new Helper();
  }

  async formCeroesStats(instance, tokensCount) {
    console.log('----------------');
    console.log(this.h.createRow(['#', 'St', 'Pr', 'Ag', 'Ma', 'Type', 'Sum']));

    for (let i = 1; i <= tokensCount; i++) {
      let cero = await instance.ceroes(i);
      cero = await this.h.solCeroToJSCero(cero);

      const sum = cero.strength + cero.protection + cero.agility + cero.magic;
      console.log(this.h.createRow(
        [i, cero.strength, cero.protection, cero.agility, cero.magic, cero.type, sum],
      ));
    }
  }
}

module.exports = HelperCreateFirst;
