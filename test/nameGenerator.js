const { use, expect } = require('chai');
const { solidity } = require('ethereum-waffle');

const CeroToken = artifacts.require('CeroToken');

const Reverter = require('./helpers/reverter');

use(solidity);
use(require('chai-as-promised')).should();

contract('NameGenerator', (accounts) => {
  const reverter = new Reverter(web3);

  const OWNER = accounts[0];
  const IVAN = accounts[2];

  let ceroToken;

  before(async () => {
    ceroToken = await CeroToken.new();

    await reverter.snapshot();
  });
  afterEach(async () => {
    await reverter.revert();
  });

  describe('setName1()', async () => {
    it('should set name1', async () => {
      await ceroToken.setName1('New name 43');
      (await ceroToken.name1(43)).should.be.equal('New name 43');

      await expect(ceroToken.setName1('New name 0', { from: IVAN }))
        .to.be.revertedWith('[E-111] - Caller is not the owner.');
    });

    it('setName2()', async () => {
      await ceroToken.setName2('New name 43');
      (await ceroToken.name2(43)).should.be.equal('New name 43');

      await expect(ceroToken.setName2('New name 0', { from: IVAN }))
        .to.be.revertedWith('[E-111] - Caller is not the owner.');
    });
  });
});
