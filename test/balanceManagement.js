const { use, expect } = require('chai');
const { solidity } = require('ethereum-waffle');

const CeroToken = artifacts.require('CeroToken');
const CreateBaseCero = artifacts.require('CreateBaseCero');

const Reverter = require('./helpers/reverter');

use(solidity);
use(require('chai-as-promised')).should();

contract('BalanceManagement', (accounts) => {
  const reverter = new Reverter(web3);

  const OWNER = accounts[0];
  const OLEG = accounts[1];

  let ceroToken;
  let createBaseCero;
  let fee;

  before(async () => {
    ceroToken = await CeroToken.new();
    createBaseCero = await CreateBaseCero.new(ceroToken.address);

    await ceroToken.generateFirstToken();
    fee = (await ceroToken.fee()).toString();

    await ceroToken.addOwnership(createBaseCero.address);

    await reverter.snapshot();
  });

  afterEach(async () => {
    await reverter.revert();
  });

  describe('getBalance()', async () => {
    it('should increase contract balance and calculate balance', async () => {
      await createBaseCero.createBaseCero(OLEG, { value: fee }); // Token 1
      await createBaseCero.createBaseCero(OLEG, { value: fee }); // Token 2

      const balance = await createBaseCero.getBalance();
      assert.equal(balance.toString(), String(fee * 2));
    });

    it('should revert if caller not the owner', async () => {
      await expect(createBaseCero.getBalance({ from: OLEG })).to.be.revertedWith('Ownable: caller is not the owner');
    });
  });

  describe('withdrawReward()', async () => {
    it('should withdraw tokens from contract', async () => {
      await createBaseCero.createBaseCero(OLEG, { value: fee }); // Token 1
      await createBaseCero.createBaseCero(OLEG, { value: fee }); // Token 2

      const balanceBefore = await web3.eth.getBalance(OWNER);

      await createBaseCero.withdrawReward();

      const balanceAfter = await web3.eth.getBalance(OWNER);
      assert.equal(balanceAfter - balanceBefore > 0, true);
    });

    it('should revert if caller not the owner', async () => {
      await expect(createBaseCero.withdrawReward({ from: OLEG }))
        .to.be.revertedWith('Ownable: caller is not the owner');
    });
  });
});
