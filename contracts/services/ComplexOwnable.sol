// SPDX-License-Identifier: MIT
pragma solidity ^0.7.2;

/// @title This contract add multiple owner for contract
/// @author Oleksandr Fedorenko
contract ComplexOwnable {
    mapping(address => bool) private owners;

    constructor() {
        owners[msg.sender] = true;
    }

    modifier onlyOwner() {
        require(owners[msg.sender], "[E-111] - Caller is not the owner.");
        _;
    }

    /// @notice Add address that can execute functions.
    /// @param _newOwner New address
    function addOwnership(address _newOwner) external onlyOwner {
        require(_newOwner != address(0), "[E-112] - New owner is the zero address.");
        owners[_newOwner] = true;
    }

    /// @notice Remove address that can execute functions.
    /// @param _oldOwner New address
    function removeOwnership(address _oldOwner) external onlyOwner {
        owners[_oldOwner] = false;
    }

    /// @notice Transfer ownership from address to address
    /// @param _oldOwner Old address
    /// @param _newOwner New address
    function transferOwnership(address _oldOwner, address _newOwner) external onlyOwner {
        require(_newOwner != address(0), "[E-113] - New owner is the zero address");
        owners[_newOwner] = true;
        owners[_oldOwner] = false;
    }

    /// @notice Check address is owner or not
    /// @param _address Address
    /// @return True if address contract owner
    function isOwner(address _address) external view returns (bool) {
        return owners[_address];
    }
}
