const { use } = require('chai');
const { solidity } = require('ethereum-waffle');

const CeroToken = artifacts.require('CeroToken');
const CreateBaseCero = artifacts.require('CreateBaseCero');

const Reverter = require('../helpers/reverter');
const Helper = require('../helpers/helper');

use(solidity);
use(require('chai-as-promised')).should();

contract('CreateBaseCero. Stress.', (accounts) => {
  const reverter = new Reverter(web3);
  const h = new Helper();
  const tokenLvl1 = 20;

  const OLEG = accounts[1];

  let ceroToken;
  let createBaseCero;
  let fee;

  before(async () => {
    ceroToken = await CeroToken.new();
    createBaseCero = await CreateBaseCero.new(ceroToken.address);

    await ceroToken.generateFirstToken();
    // fee = (await ceroToken.fee()).toString();

    await ceroToken.addOwnership(createBaseCero.address);

    await reverter.snapshot();
  });
  afterEach(async () => {
    await reverter.revert();
  });

  describe('createBaseCero()', async () => {
    beforeEach(async () => {
      for (let i = 0; i < tokenLvl1; i++) {
        await createBaseCero.createBaseCero(OLEG, { value: fee });
      }
    });
    it('check point sum', async () => {
      console.log(h.createRow(['#', 'Name', 'St', 'Pr', 'Ag', 'Ma', 'Type', 'Sum'], 24));

      for (let i = 1; i <= tokenLvl1; i++) {
        let cero = await ceroToken.ceroes(i);
        cero = await h.solCeroToJSCero(cero);

        const sum = cero.strength + cero.protection + cero.agility + cero.magic;

        console.log(h.createRow(
          [`#${i}`, cero.name, cero.strength, cero.protection, cero.agility, cero.magic, cero.type, sum], 24,
        ));

        assert.isAtLeast(sum, 14);
        assert.isAtMost(sum, 26);

        assert.isAbove(cero.name.length, 0);
        assert.equal(cero.lvl, 1);
        assert.equal(cero.parent1, 0);
        assert.equal(cero.parent2, 0);
        assert.equal(cero.isChild, true);
        assert.isAbove(cero.birthday, 1600000000);
        assert.equal(cero.experience, 8);
      }
    });
  });
});
