const { use, expect } = require('chai');
const { solidity } = require('ethereum-waffle');

const CeroToken = artifacts.require('CeroToken');
const CreateBaseCero = artifacts.require('CreateBaseCero');
const CreateChildCero = artifacts.require('CreateChildCero');
const LevelUp = artifacts.require('LevelUp');
const Fight = artifacts.require('Fight');

const Reverter = require('./helpers/reverter');
const Helper = require('./helpers/helper');

use(solidity);
use(require('chai-as-promised')).should();

contract('LevelUp', (accounts) => {
  const reverter = new Reverter(web3);
  const h = new Helper();

  const OLEG = accounts[1];
  const IVAN = accounts[2];

  let ceroToken;
  let createBaseCero;
  let createChildCero;
  let levelUp;
  let fight;
  let fee;

  before(async () => {
    ceroToken = await CeroToken.new();
    createBaseCero = await CreateBaseCero.new(ceroToken.address);
    createChildCero = await CreateChildCero.new(ceroToken.address);
    levelUp = await LevelUp.new(ceroToken.address);
    fight = await Fight.new(ceroToken.address, createBaseCero.address);

    await ceroToken.generateFirstToken();
    fee = (await ceroToken.fee()).toString();

    await ceroToken.addOwnership(createBaseCero.address);
    await ceroToken.addOwnership(createChildCero.address);
    await ceroToken.addOwnership(levelUp.address);
    await ceroToken.addOwnership(fight.address);

    await reverter.snapshot();
  });

  afterEach(async () => {
    await reverter.revert();
  });

  describe('levelUp()', async () => {
    beforeEach(async () => {
      await createBaseCero.createBaseCero(OLEG, { value: fee }); // Token 1
      await createBaseCero.createBaseCero(IVAN, { value: fee }); // Token 2
    });

    it('should revert when not a token owner', async () => {
      await expect(levelUp.levelUp(1, 0, 0, 0, 0, { from: IVAN, value: fee }))
        .to.be.revertedWith('[ELU-449] - Not a token owner.');
    });

    it('should revert when not enough experience', async () => {
      await expect(levelUp.levelUp(1, 3, 3, 3, 4, { from: OLEG, value: fee }))
        .to.be.revertedWith('[ELU-451] - Not enough experience.');
    });

    it('should revert when point does not match with distribution', async () => {
      await expect(levelUp.levelUp(1, 0, 0, 0, 0, { from: OLEG, value: fee }))
        .to.be.revertedWith('[ELU-450] - The sum of points for distribution does not match.');
    });

    it('should revert when have descendants', async () => {
      await createBaseCero.createBaseCero(OLEG, { value: fee }); // Token 3

      await createChildCero.createChildCero(1, 3, { from: OLEG, value: fee }); // Token 4

      await expect(levelUp.levelUp(1, 3, 3, 3, 4, { from: OLEG, value: fee }))
        .to.be.revertedWith('[ELU-452] - There should be no descendants.');
      await expect(levelUp.levelUp(3, 3, 3, 3, 4, { from: OLEG, value: fee }))
        .to.be.revertedWith('[ELU-452] - There should be no descendants.');
    });

    it('should successful levelUp', async () => {
      for (let i = 0; i < 3; i++) {
        await fight.fight(1, { value: fee });
      }

      const token1 = h.solCeroToJSCero(await ceroToken.ceroes(1));
      const token2 = h.solCeroToJSCero(await ceroToken.ceroes(2));
      await levelUp.levelUp(1, 3, 3, 3, 4, { from: OLEG, value: fee });
      await levelUp.levelUp(2, 0, 0, 13, 0, { from: IVAN, value: fee });
      const token1AU = h.solCeroToJSCero(await ceroToken.ceroes(1));
      const token2AU = h.solCeroToJSCero(await ceroToken.ceroes(2));

      assert.equal(token1AU.lvl, 2);
      assert.oneOf(token1AU.strength, [0, token1.strength + 3]);
      assert.equal(token1AU.protection, token1.protection + 3);
      assert.equal(token1AU.agility, token1.agility + 3);
      assert.oneOf(token1AU.magic, [0, token1.magic + 4]);

      assert.equal(token2AU.lvl, 2);
      assert.equal(token2AU.strength, token2.strength);
      assert.equal(token2AU.protection, token2.protection);
      assert.equal(token2AU.agility, token2.agility + 13);
      assert.equal(token2AU.magic, token2.magic);
    });

    it('should revert if not enough funds', async () => {
      await expect(levelUp.levelUp(1, 3, 3, 3, 4, { from: OLEG }))
        .to.be.revertedWith('[ELU-448] - Insufficient funds.');
    });
  });
});
