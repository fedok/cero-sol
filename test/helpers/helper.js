class Helper {
  constructor() {
    this.warrior = 'warrior';
    this.thief = 'thief';
    this.magic = 'magic';
  }

  solCeroToJSCero(cero) {
    const ceroL = {
      name: cero.name,
      lvl: cero.lvl.toNumber(),
      strength: cero.strength.toNumber(),
      protection: cero.protection.toNumber(),
      agility: cero.agility.toNumber(),
      magic: cero.magic.toNumber(),
      parent1: cero.parent1.toNumber(),
      parent2: cero.parent2.toNumber(),
      isChild: cero.isChild,
      birthday: cero.birthday.toNumber(),
      experience: cero.experience.toNumber(),
      type: this.getCeroType(cero),
    };

    ceroL.type = this.getCeroType(ceroL);

    return ceroL;
  }

  getCeroType(cero) {
    const warriorPoint = cero.strength + cero.protection;
    const thiefPoint = cero.strength + cero.agility;
    const magicPoint = cero.agility + cero.magic;

    if (warriorPoint >= thiefPoint && warriorPoint > magicPoint) {
      return this.warrior;
    }
    if (thiefPoint >= warriorPoint && thiefPoint > magicPoint) {
      return this.thief;
    }

    return this.magic;
  }

  // eslint-disable-next-line class-methods-use-this
  solFightResultToFightResult(fightResult) {
    return {
      attacker: fightResult.attacker.toNumber(),
      defender: fightResult.defender.toNumber(),
      winner: fightResult.winner.toNumber(),
    };
  }

  // eslint-disable-next-line class-methods-use-this
  createCell(val, length) {
    let txt = `| ${String(val).trim()}`;

    while (txt.length < length) {
      txt += ' ';
    }
    txt += ' |';
    return txt;
  }

  createRow(array, length = 9) {
    let row = '';
    array.forEach((item, key) => {
      row += this.createCell(item, length);
      if (key !== array.length - 1) {
        row = row.slice(0, -1);
      }
    });

    return row;
  }

  calcPercent(total, value) {
    return this.round((value * 100) / total);
  }

  // eslint-disable-next-line class-methods-use-this
  round(value, round = 2) {
    const roundTo = 10 ** round;
    return Math.round(value * roundTo) / roundTo;
  }

  getExperienceCountForLevelUp(lvl) {
    return this.getFibonacciNumber(lvl + 5);
  }

  getFibonacciNumber(lvl) {
    if (lvl === 0) {
      return 0;
    }
    if (lvl === 1 || lvl === 2) {
      return 1;
    }
    return this.getFibonacciNumber(lvl - 1) + this.getFibonacciNumber(lvl - 2);
  }

  // eslint-disable-next-line class-methods-use-this
  getRandomInt(max) {
    return Math.floor(Math.random() * max);
  }
}

module.exports = Helper;
