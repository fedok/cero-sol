// SPDX-License-Identifier: MIT
pragma solidity ^0.7.2;
pragma experimental ABIEncoderV2;

import "./interfaces/ICeroStruct.sol";
import "./CreateBaseCero.sol";
import "./CeroToken.sol";
import "./services/Randomizer.sol";
import "./services/Globals.sol";
import "./services/BalanceManagement.sol";

/// @title This contract describes fight process between two tokens.
/// @author Oleksandr Fedorenko
contract Fight is ICeroStruct, Randomizer, Globals, BalanceManagement {
    /// @notice Contract address which contain tokens and tokens data.
    CeroToken public tokenContract;

    /// @notice Contract address which contain logic for base token creation
    CreateBaseCero public createBaseCeroContract;

    constructor(address _tokenContract, address _createBaseCeroContract) {
        tokenContract = CeroToken(_tokenContract);
        createBaseCeroContract = CreateBaseCero(_createBaseCeroContract);
    }

    event FightResult(
        uint256 indexed attacker,
        uint256 indexed defender,
        uint256 indexed winner,
        bool isNewTokenAccepted,
        uint256 time
    );

    /// @notice Fight log structure.
    /// @param attacker Attacker token num
    /// @param defender Defender token num
    /// @param winner Winner token num
    /// @param time Fight timestamp
    struct FightLog {
        uint256 attacker;
        uint256 defender;
        uint256 winner;
        uint64 time;
    }

    /// @notice Contain all fight logs for some token.
    mapping(uint256 => FightLog[]) public fightLogs;

    /// @notice Token HP amount per each level.
    uint256 public baseHP = 50;

    /// @notice Token HP additional amount per each protection point.
    uint256 public baseHPIncrease = 3;

    /// @notice Describes fight process between tokens. Add experience to token after fight.
    /// @param _attackerNum Attacker token number
    function fight(uint256 _attackerNum) external {
        Cero memory _attacker = tokenDataToTokenStruct(_attackerNum);
        require(_attacker.isChild, "[EF-602] - There should be no descendants.");

        uint256 _defenderNum = _getDefenderNum(_attacker.lvl, _attackerNum);
        Cero memory _defender = tokenDataToTokenStruct(_defenderNum);

        FightLog memory _fightLog = FightLog(_attackerNum, _defenderNum, 0, uint64(block.timestamp));

        uint256 _expForNextLevel = getExperienceCountForLevelUp(_attacker.lvl + 1);
        bool _isNewTokenAccepted = false;

        if (_isAttackerWin(_attacker, _defender)) {
            if (_attacker.experience + 5 > _expForNextLevel) {
                tokenContract.updateTokenExperience(_attackerNum, _expForNextLevel);
            } else {
                tokenContract.updateTokenExperience(_attackerNum, _attacker.experience + 5);
            }

            if (_defender.experience + 2 > _expForNextLevel) {
                tokenContract.updateTokenExperience(_defenderNum, _expForNextLevel);
            } else {
                tokenContract.updateTokenExperience(_defenderNum, _defender.experience + 2);
            }

            _fightLog.winner = _attackerNum;

            if (random(100) < 50) {
                _isNewTokenAccepted = true;
                address _attackerOwner = tokenContract.ownerOf(_attackerNum);
                uint256 _attackerOwnerAcceptedToCreateBaseTokens =
                    tokenContract.acceptedToCreateBaseToken(_attackerOwner);
                tokenContract.setAcceptedToCreateBaseToken(
                    _attackerOwner,
                    _attackerOwnerAcceptedToCreateBaseTokens + 1
                );
            }
        } else {
            if (_attacker.experience + 2 > _expForNextLevel) {
                tokenContract.updateTokenExperience(_attackerNum, _expForNextLevel);
            } else {
                tokenContract.updateTokenExperience(_attackerNum, _attacker.experience + 2);
            }

            if (_defender.experience + 5 > _expForNextLevel) {
                tokenContract.updateTokenExperience(_defenderNum, _expForNextLevel);
            } else {
                tokenContract.updateTokenExperience(_defenderNum, _defender.experience + 5);
            }

            _fightLog.winner = _defenderNum;
        }

        fightLogs[_attackerNum].push(_fightLog);
        fightLogs[_defenderNum].push(_fightLog);

        emit FightResult(_attackerNum, _defenderNum, _fightLog.winner, _isNewTokenAccepted, block.timestamp);
    }

    /// @notice Get random defender for attacker token.
    /// @param _attackerLvl Attacker token level
    /// @return Defender token number
    function _getDefenderNum(uint8 _attackerLvl, uint256 _attackerNum) private view returns (uint256) {
        uint256 _defenderNum = 0;
        uint256 _tokenCount = tokenContract.getTokensCount();

        uint256 _startFromNum = random(_tokenCount, block.timestamp);

        for (uint256 i = 0; i < _tokenCount; i++) {
            uint256 _num = _startFromNum + i;
            if (_num >= _tokenCount) {
                _num = _num - _tokenCount + 1;
            }

            (uint8 _lvl, bool _isChild) = tokenContract.getTokenShortDataType3(_num);
            if (_num != _attackerNum && _lvl == _attackerLvl && _isChild == true) {
                _defenderNum = _num;
                break;
            }
        }

        require(_defenderNum != 0, "[EF-603] - Opponent not found.");

        return _defenderNum;
    }

    /// @notice Describe fight process.
    /// @param _attacker Attacker token
    /// @param _defender Defender token
    /// @return _attackerWin True if attacker win fight
    function _isAttackerWin(Cero memory _attacker, Cero memory _defender) private view returns (bool _attackerWin) {
        uint256 _attackerHP = baseHP * _attacker.lvl + baseHPIncrease * _attacker.protection;
        uint256 _defenderHP = baseHP * _defender.lvl + baseHPIncrease * _defender.protection;

        uint256 _attackerDamage = _getDamage(_attacker, _defender);
        uint256 _defenderDamage = _getDamage(_defender, _attacker);

        for (uint256 i = 0; i < 10000; i++) {
            if (i % 2 == 0) {
                uint256 _dodgeChance =
                    random(_defender.strength + _defender.protection + _defender.agility + _defender.magic, i);
                if (_dodgeChance < _getDodgeValue(_attacker, _defender)) {
                    continue;
                }

                if (_attackerDamage >= _defenderHP) {
                    _attackerWin = true;
                    break;
                } else {
                    _defenderHP -= _attackerDamage;
                }
            } else {
                uint256 _dodgeChance =
                    random(_attacker.strength + _attacker.protection + _attacker.agility + _attacker.magic, i);
                if (_dodgeChance < _getDodgeValue(_defender, _attacker)) {
                    continue;
                }

                if (_defenderDamage >= _attackerHP) {
                    _attackerWin = false;
                    break;
                } else {
                    _attackerHP -= _defenderDamage;
                }
            }
        }

        return _attackerWin;
    }

    /// @notice Calculate damage from attacker to defender.
    /// @param _attacker Attacker token number
    /// @param _defender Defender token number
    /// @return Damage
    function _getDamage(Cero memory _attacker, Cero memory _defender) private pure returns (uint256) {
        // Magic attack ignore more protection then strength.
        if (_attacker.strength == 0) {
            return _attacker.magic > _defender.protection / 3 ? _attacker.magic - _defender.protection / 3 : 0;
        } else {
            return _attacker.strength > _defender.protection / 2 ? _attacker.strength - _defender.protection / 2 : 0;
        }
    }

    /// @notice Calculate dodge value for defender
    /// @param _attacker Attacker token number
    /// @param _defender Defender token number
    /// @return Dodge value
    function _getDodgeValue(Cero memory _attacker, Cero memory _defender) private pure returns (uint256) {
        uint256 attWarPoint = _attacker.strength + _attacker.protection;
        uint256 attThiefPoint = _attacker.strength + _attacker.agility;
        uint256 defWarPoint = _defender.strength + _defender.protection;
        uint256 defThiefPoint = _defender.strength + _defender.agility;

        // If attacker is warrior and defender is thief
        if (_attacker.magic == 0 && attWarPoint > attThiefPoint && defThiefPoint > defWarPoint) {
            return _defender.agility + _defender.agility / 3;
        }

        // If attacker is wizard and defender is thief
        if (_attacker.magic != 0 && defThiefPoint > defWarPoint) {
            return _defender.agility + _defender.agility / 3;
        }

        // If attacker is wizard and defender is warrior
        if (_attacker.magic != 0 && defWarPoint > defThiefPoint) {
            return _defender.agility + _defender.agility / 2;
        }

        return _defender.agility;
    }

    /// @notice Return all logs for selected token.
    /// @return _logs Array of FightLog structure
    function getLogsForToken(uint256 _tokenNum) external view returns (FightLog[] memory _logs) {
        uint256 _logsLength = fightLogs[_tokenNum].length;
        _logs = new FightLog[](_logsLength);

        for (uint256 i = 0; i < _logsLength; i++) {
            _logs[i] = fightLogs[_tokenNum][i];
        }
    }

    /// @notice Transform data from token contract to memory struct.
    /// @param _tokenNum Token number
    /// @return _token Token data
    function tokenDataToTokenStruct(uint256 _tokenNum) private view returns (Cero memory _token) {
        (uint16 _st, uint16 _ag, uint16 _pr, uint16 _ma, uint8 _lvl, bool _isChild, uint256 _exp) =
            tokenContract.getTokenShortDataType2(_tokenNum);

        _token = Cero({
            name: "",
            lvl: _lvl,
            strength: _st,
            protection: _pr,
            agility: _ag,
            magic: _ma,
            parent1: 0,
            parent2: 0,
            isChild: _isChild,
            birthday: 0,
            experience: _exp
        });
    }
}
