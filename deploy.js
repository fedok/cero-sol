const CeroToken = artifacts.require('CeroToken');
const CreateBaseCero = artifacts.require('CreateBaseCero');
const CreateChildCero = artifacts.require('CreateChildCero');
const LevelUp = artifacts.require('LevelUp');
const Fight = artifacts.require('Fight');

module.exports = async (callback) => {
  console.log('Begin deploy!\n');

  try {
    const ceroToken = await CeroToken.new();
    console.log('CeroToken:', ceroToken.address);

    const createBaseCero = await CreateBaseCero.new(ceroToken.address);
    console.log('CreateBaseCero:', createBaseCero.address);

    const createChildCero = await CreateChildCero.new(ceroToken.address);
    console.log('CreateChildCero:', createChildCero.address);

    const levelUp = await LevelUp.new(ceroToken.address);
    console.log('LevelUp:', levelUp.address);

    const fight = await Fight.new(ceroToken.address, createBaseCero.address);
    console.log('Fight:', fight.address);

    await ceroToken.generateFirstToken();

    await ceroToken.addOwnership(createBaseCero.address);
    await ceroToken.addOwnership(createChildCero.address);
    await ceroToken.addOwnership(levelUp.address);
    await ceroToken.addOwnership(fight.address);

    console.log('\nDone!');
  } catch (e) {
    console.log(e.message);
  }

  callback();
};
