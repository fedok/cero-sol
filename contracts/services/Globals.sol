// SPDX-License-Identifier: MIT
pragma solidity ^0.7.2;

/// @title This contract contain common function for project.
/// @author Oleksandr Fedorenko
contract Globals {
    /// @notice Calculate experience for token level, not less then 8
    /// @param _lvl Token level
    /// @return Experience count
    function getExperienceCountForLevelUp(uint8 _lvl) internal pure returns (uint256) {
        return getFibonacciNumber(_lvl + 5);
    }

    /// @notice Calculate experience for token level up
    /// @param _lvl Token level
    /// @return Experience count
    function getFibonacciNumber(uint8 _lvl) private pure returns (uint256) {
        if (_lvl == 0) {
            return 0;
        }
        if (_lvl == 1 || _lvl == 2) {
            return 1;
        }
        return getFibonacciNumber(_lvl - 1) + getFibonacciNumber(_lvl - 2);
    }
}
