const { use, expect } = require('chai');
const { solidity } = require('ethereum-waffle');

const CeroToken = artifacts.require('CeroToken');

const Reverter = require('./helpers/reverter');
const Helper = require('./helpers/helper');

use(solidity);
use(require('chai-as-promised')).should();

contract('CeroToken', (accounts) => {
  const reverter = new Reverter(web3);
  const h = new Helper();

  const OLEG = accounts[1];
  const IVAN = accounts[2];

  let ceroToken;

  before(async () => {
    ceroToken = await CeroToken.new();

    await reverter.snapshot();
  });

  afterEach(async () => {
    await reverter.revert();
  });

  describe('setTokenURI()', async () => {
    it('should set token URI', async () => {
      await ceroToken.createToken(OLEG, 1, 2, 3, 4, 5, 6, 7, true); // Token 0

      await ceroToken.setTokenURI(0, 'abcd', { from: OLEG });
      const tokenURI = await ceroToken.tokenURI(0);

      assert.equal(tokenURI, 'abcd');
    });
    it('should revert if caller not a token owner', async () => {
      await ceroToken.createToken(OLEG, 1, 2, 3, 4, 5, 6, 7, true); // Token 0

      await expect(ceroToken.setTokenURI(0, 'ada'))
        .to.be.revertedWith('[ECT-187] - Only token owner can set token description.');
    });
  });

  describe('generateFirstToken()', async () => {
    it('should crate first token', async () => {
      await ceroToken.generateFirstToken();
    });

    it('should revert if caller not a owner', async () => {
      await expect(ceroToken.generateFirstToken({ from: OLEG }))
        .to.be.revertedWith('[E-111] - Caller is not the owner.');
    });

    it('should revert if first token already generated', async () => {
      await ceroToken.generateFirstToken();
      await expect(ceroToken.generateFirstToken())
        .to.be.revertedWith('[ECT-101] - First token already generated.');
    });
  });

  describe('getTokensCount()', async () => {
    it('should calculate toke count', async () => {
      await ceroToken.generateFirstToken();
      assert.equal((await ceroToken.getTokensCount()).toString(), '1');
    });
  });

  describe('createToken()', async () => {
    it('should create token', async () => {
      await ceroToken.createToken(OLEG, 1, 2, 3, 4, 5, 6, 7, true); // Token 0

      const token = h.solCeroToJSCero(await ceroToken.ceroes(0));
      const tokenOwner = await ceroToken.ownerOf(0);

      assert.equal(tokenOwner, OLEG);
      assert.equal(token.lvl, 1);
      assert.equal(token.strength, 2);
      assert.equal(token.protection, 3);
      assert.equal(token.agility, 4);
      assert.equal(token.magic, 5);
      assert.equal(token.parent1, 6);
      assert.equal(token.parent2, 7);
      assert.equal(token.isChild, true);
      assert.equal(token.name.length > 0, true);
      assert.equal(token.experience, 8);
      assert.equal(token.birthday > 0, true);
    });

    it('should revert if caller not a owner', async () => {
      await expect(ceroToken.createToken(OLEG, 1, 2, 3, 4, 5, 6, 7, true, { from: OLEG }))
        .to.be.revertedWith('[E-111] - Caller is not the owner.');
    });
  });

  describe('setAcceptedToCreateBaseToken()', async () => {
    it('should set new token count for address', async () => {
      const acceptedBefore = await ceroToken.acceptedToCreateBaseToken(OLEG);
      await ceroToken.setAcceptedToCreateBaseToken(OLEG, 100);
      const acceptedAfter = await ceroToken.acceptedToCreateBaseToken(OLEG);

      assert.equal(acceptedAfter.toNumber(), acceptedBefore.toNumber() + 100);
    });

    it('should revert if caller not a owner', async () => {
      await expect(ceroToken.setAcceptedToCreateBaseToken(OLEG, 100, { from: OLEG }))
        .to.be.revertedWith('[E-111] - Caller is not the owner.');
    });
  });

  describe('updateTokenChildStatus()', async () => {
    it('should change token child status', async () => {
      await ceroToken.createToken(OLEG, 1, 2, 3, 4, 5, 6, 7, true); // Token 0

      await ceroToken.updateTokenChildStatus(0, false);
      const token1 = h.solCeroToJSCero(await ceroToken.ceroes(0));
      assert.equal(token1.isChild, false);

      await ceroToken.updateTokenChildStatus(0, true);
      const token2 = h.solCeroToJSCero(await ceroToken.ceroes(0));
      assert.equal(token2.isChild, true);
    });

    it('should revert if caller not a owner', async () => {
      await expect(ceroToken.updateTokenChildStatus(1, false, { from: OLEG }))
        .to.be.revertedWith('[E-111] - Caller is not the owner.');
    });
  });

  describe('updateTokenExperience()', async () => {
    it('should set new token experience', async () => {
      await ceroToken.createToken(OLEG, 1, 2, 3, 4, 5, 6, 7, true); // Token 0

      await ceroToken.updateTokenExperience(0, 111);
      const token = h.solCeroToJSCero(await ceroToken.ceroes(0));
      assert.equal(token.experience, 111);
    });

    it('should revert if caller not a owner', async () => {
      await expect(ceroToken.updateTokenExperience(1, 1, { from: OLEG }))
        .to.be.revertedWith('[E-111] - Caller is not the owner.');
    });
  });

  describe('updateTokenAfterLevelUp()', async () => {
    it('should set new token data after level up', async () => {
      await ceroToken.createToken(OLEG, 1, 2, 3, 4, 5, 6, 7, true); // Token 0

      await ceroToken.updateTokenAfterLevelUp(0, 11, 12, 13, 15, 100);
      const token = h.solCeroToJSCero(await ceroToken.ceroes(0));

      assert.equal(token.lvl, 100);
      assert.equal(token.strength, 11);
      assert.equal(token.protection, 12);
      assert.equal(token.agility, 13);
      assert.equal(token.magic, 15);
    });

    it('should revert if caller not a owner', async () => {
      await expect(ceroToken.updateTokenAfterLevelUp(1, 11, 12, 13, 15, 100, { from: OLEG }))
        .to.be.revertedWith('[E-111] - Caller is not the owner.');
    });
  });

  describe('getTokenShortDataType1', async () => {
    it('should return valid token data', async () => {
      await ceroToken.createToken(OLEG, 1, 2, 3, 4, 5, 6, 7, true); // Token 0

      const data = await ceroToken.getTokenShortDataType1(0);
      const token = h.solCeroToJSCero(await ceroToken.ceroes(0));

      assert.equal(token.strength, data[0].toNumber());
      assert.equal(token.protection, data[1].toNumber());
      assert.equal(token.agility, data[2].toNumber());
      assert.equal(token.magic, data[3].toNumber());
      assert.equal(token.lvl, data[4].toNumber());
      assert.equal(token.isChild, data[5]);
    });
  });

  describe('getTokenShortDataType2', async () => {
    it('should return valid token data', async () => {
      await ceroToken.createToken(OLEG, 1, 2, 3, 4, 5, 6, 7, true); // Token 0

      const data = await ceroToken.getTokenShortDataType2(0);
      const token = h.solCeroToJSCero(await ceroToken.ceroes(0));

      assert.equal(token.strength, data[0].toNumber());
      assert.equal(token.protection, data[1].toNumber());
      assert.equal(token.agility, data[2].toNumber());
      assert.equal(token.magic, data[3].toNumber());
      assert.equal(token.lvl, data[4].toNumber());
      assert.equal(token.isChild, data[5]);
      assert.equal(token.experience, data[6].toNumber());
    });
  });

  describe('getTokenShortDataType3', async () => {
    it('should return valid token data', async () => {
      await ceroToken.createToken(OLEG, 1, 2, 3, 4, 5, 6, 7, true); // Token 0

      const data = await ceroToken.getTokenShortDataType3(0);
      const token = h.solCeroToJSCero(await ceroToken.ceroes(0));

      assert.equal(token.lvl, data[0].toNumber());
      assert.equal(token.isChild, data[1]);
    });
  });

  describe('getAllTokens()', async () => {
    it('should return all tokens', async () => {
      for (let i = 0; i < 5; i++) {
        await ceroToken.createToken(OLEG, 1, 2, 3, 4, 5, 6, 7, true);
      }
      const res = await ceroToken.getAllTokens();
      assert.equal(res.length, 5);
    });
  });

  describe('getTokensByAddress()', async () => {
    it('should return all tokens by owner address', async () => {
      await ceroToken.createToken(IVAN, 1, 2, 3, 4, 5, 6, 7, true); // Token 0

      for (let i = 0; i < 2; i++) {
        await ceroToken.createToken(OLEG, 1, 2, 3, 4, 5, 6, 7, true); // Token 1, 2
      }
      const res = await ceroToken.getTokensByAddress(OLEG);
      assert.equal(res['_ceroes'].length, 2);
      assert.equal(res['_ids'].length, 2);
      assert.equal(res['_ids'][0], 1);
      assert.equal(res['_ids'][1], 2);
    });
  });
});
