const { use } = require('chai');
const { solidity } = require('ethereum-waffle');
const truffleAssert = require('truffle-assertions');

const CeroToken = artifacts.require('CeroToken');
const CreateBaseCero = artifacts.require('CreateBaseCero');
const CreateChildCero = artifacts.require('CreateChildCero');
const LevelUp = artifacts.require('LevelUp');
const Fight = artifacts.require('Fight');

const Reverter = require('../helpers/reverter');
const Helper = require('../helpers/helper');
const HelperFight = require('../helpers/helper.fight');

use(solidity);
use(require('chai-as-promised')).should();

contract('Fight. Stress', (accounts) => {
  const reverter = new Reverter(web3);
  const h = new Helper();
  const hf = new HelperFight();

  const OLEG = accounts[1];
  const IVAN = accounts[2];

  let ceroToken;
  let createBaseCero;
  let createChildCero;
  let levelUp;
  let fight;
  let fee;

  before(async () => {
    ceroToken = await CeroToken.new();
    createBaseCero = await CreateBaseCero.new(ceroToken.address);
    createChildCero = await CreateChildCero.new(ceroToken.address);
    levelUp = await LevelUp.new(ceroToken.address);
    fight = await Fight.new(ceroToken.address, createBaseCero.address);

    await ceroToken.generateFirstToken();
    fee = (await ceroToken.fee()).toString();

    await ceroToken.addOwnership(createBaseCero.address);
    await ceroToken.addOwnership(createChildCero.address);
    await ceroToken.addOwnership(levelUp.address);
    await ceroToken.addOwnership(fight.address);

    await reverter.snapshot();
  });
  afterEach(async () => {
    await reverter.revert();
  });

  describe('Fight stress test', async () => {
    const tokensCount = 4;
    const fightsCount = 10;

    beforeEach(async () => {
      for (let i = 0; i < tokensCount; i++) {
        if (i % 2 === 0) {
          await createBaseCero.createBaseCero(OLEG, { value: fee });
        } else {
          await createBaseCero.createBaseCero(IVAN, { value: fee });
        }
      }
    });

    it('fight results', async () => {
      // await hcf.formCeroesStats(instance, tokensCount);

      const fightResult = [];

      for (let i = 1; i <= fightsCount; i++) {
        const tokenNum = h.getRandomInt(tokensCount);
        const fightResp = await fight.fight(tokenNum === 0 ? 1 : tokenNum, { value: fee });
        truffleAssert.eventEmitted(fightResp, 'FightResult', (event) => {
          fightResult.push(event);
          return true;
        });
      }

      // hf.formFightResultInfo(fightResult);
      await hf.formFightStats(ceroToken, fightResult);
    });
  });
});
